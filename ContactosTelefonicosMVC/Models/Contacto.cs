﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace ContactosTelefonicosMVC.Models
{
    [Table("Contactos")]
    public class Contacto
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(50, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 3)]
        public string Nombre { get; set; }

        [Display(Name = "Apellido paterno")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(50, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 3)]
        public string ApellidoPaterno { get; set; }

        [Display(Name = "Apellido materno")]
        [StringLength(50, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 3)]
        public string ApellidoMaterno { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(15, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 8)]
        public string Telefono { get; set; }

        [DataType(DataType.EmailAddress, ErrorMessage = "Favor de ingresar un correo válido.")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(70, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 6)]
        public string Correo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(80, ErrorMessage = "El campo {0} debe estar entre {2} y {1} caractres.", MinimumLength = 5)]
        public virtual string Foto { get; set; }
    }
}