﻿using ContactosTelefonicosMVC.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ContactosTelefonicosMVC.ViewModels
{
    public class ContactoViewModel : Contacto
    {
        [NotMapped]
        [Required(ErrorMessage = "El campo Foto es requerido.")]
        public HttpPostedFileBase PostedFile { get; set; }
        public override string Foto { get { return PostedFile.FileName; } }
    }
}